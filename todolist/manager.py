import logging
import os
import os.path

from configparser import ConfigParser, ExtendedInterpolation
from inspect import isclass
from os.path import expanduser, join as path_join
from tempfile import TemporaryDirectory, mkstemp

from todolist import entity_list, storage


class Manager:

    DEFAULT_CONFIG_DIR = expanduser('~/.todolist')
    DEFAULT_CONFIG_FILE = 'todolist.conf'

    def __init__(self, config_file=''):
        if config_file == '':
            config_file = path_join(
                    Manager.DEFAULT_CONFIG_DIR, 
                    Manager.DEFAULT_CONFIG_FILE) 
            self._init_config()

        logging.info('reading config file ' + expanduser(config_file))
        config = ConfigParser(interpolation=ExtendedInterpolation())
        config.read(expanduser(config_file))

        logging.info('init storage...')
        self._init_storage(config)
        self.todo_fn = self.storage.get_fn(config['general']['todo_file'])
        self.done_fn = self.storage.get_fn(config['archiving']['done_file'])
        logging.info('storage init done.')

    def _init_config(self):
        if not os.path.isdir(Manager.DEFAULT_CONFIG_DIR):
            os.mkdir(Manager.DEFAULT_CONFIG_DIR)

        config_file = path_join(
                Manager.DEFAULT_CONFIG_DIR, 
                Manager.DEFAULT_CONFIG_FILE)
        if not os.path.isfile(config_file):
            sample_config = os.path.join(
                    os.path.dirname(__file__), 
                    'sample.conf') 

            with open(config_file, 'w') as output_file:
                with open(sample_config, 'r') as input_file:
                    for line in input_file:
                        output_file.write(line)

    def _init_storage(self, config):
        provider = config['general']['storage']
        module_name = storage.Storage.__module__ + '.' + provider
        module = __import__(module_name, fromlist=['storage'])
        logging.info('using ' + module_name + ' as storage provider')

        for attr in [getattr(module, i) for i in dir(module)]:
            if isclass(attr) and issubclass(attr, storage.Storage):
                self.storage = attr(config[provider])

                return

        logging.error('invalid storage provider! abort')
        exit(1)

    def do_stuff(self):
        with TemporaryDirectory() as work_dir:
            logging.info('read current todo')
            tmp_fn = mkstemp(dir=work_dir)[1]
            self.storage.get_file(self.todo_fn, tmp_fn)
            todo_entities = entity_list.read_file(tmp_fn)

            logging.info('read current done')
            tmp_fn = mkstemp(dir=work_dir)[1]
            self.storage.get_file(self.done_fn, tmp_fn)
            done_entities = entity_list.read_file(tmp_fn)
            
            # do stuff
            new_todo_entities = []
            new_done_entities = []

            logging.info('archiving from todo')
            for entity in todo_entities:
                if entity.is_done():
                    new_done_entities.append(entity)
                else:
                    new_todo_entities.append(entity)

            new_done_entities.extend(done_entities)
            
            logging.info('write back todo')
            new_todo_fn = mkstemp(dir=work_dir)[1]
            entity_list.write_file(new_todo_fn, new_todo_entities)
            self.storage.put_file(new_todo_fn, self.todo_fn)

            logging.info('write back done')
            new_done_fn = mkstemp(dir=work_dir)[1]
            entity_list.write_file(new_done_fn, new_done_entities)
            self.storage.put_file(new_done_fn, self.done_fn)
