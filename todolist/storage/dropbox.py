import logging
import os.path

from os.path import expanduser
from dropbox.client import DropboxClient, DropboxOAuth2FlowNoRedirect

from todolist.storage import Storage


class DropboxStorage(Storage):

    def __init__(self, config_section):
        logging.info('init dropbox storage')

        self.base_dir = config_section['base_dir']
        self.app_key = config_section['app_key']
        self.app_secret = config_section['app_secret']
        self.user_token_fn = expanduser(config_section['user_token_file'])

        token = self._read_token()

        if token is None:
            token = self._ask_user_to_auth()
            self._write_token(token)

        self.client = DropboxClient(token)

    def get_file(self, source_fn, dest_fn):
        logging.info('get file ' + source_fn + ' from dropbox to ' + dest_fn)

        with open(dest_fn, 'wb') as dest_file:
            with self.client.get_file(
                    os.path.join(self.base_dir, source_fn)) as source_file:
                dest_file.write(source_file.read())

    def put_file(self, source_fn, dest_fn):
        logging.info('put file ' + source_fn + ' to dropbox ' + dest_fn)

        with open(source_fn, 'rb') as source_file:
            self.client.put_file(
                os.path.join(self.base_dir, dest_fn), source_file)

    def _init_dropbox_client(self, token):
        logging.info('init dropbox client')
        self.client = DropboxClient(token)

    def _ask_user_to_auth(self):
        logging.info('asking user for token...')
        flow = DropboxOAuth2FlowNoRedirect(self.app_key, self.app_secret)
        authorize_url = flow.start()

        print(authorize_url)
        code = input('> ').strip()

        token, user = flow.finish(code)
        return token

    def _read_token(self):
        logging.info('load user token')
        if os.path.exists(self.user_token_fn):
            with open(self.user_token_fn, 'r') as token_file:
                return token_file.readline()

        logging.warn('token not found')
        return None

    def _write_token(self, token):
        logging.info('save user token')
        with open(self.user_token_fn, 'w') as token_file:
            token_file.write(token)

    def get_fn(self, fn):
        # very bad, very ugly
        return self.base_dir + fn
