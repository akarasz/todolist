class Storage:

    def get_file(self, source_fn, dest_fn):
        raise NotImplementedError()

    def put_file(self, source_fn, dest_fn):
        raise NotImplementedError()

    def get_fn(self, fn):
        raise NotImplementedError()
