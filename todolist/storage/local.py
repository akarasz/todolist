import logging
import os.path

from todolist.storage import Storage


class LocalStorage(Storage):

    def __init__(self, config_section):
        logging.info('init local storage')

        self.base_dir = config_section['base_dir']

    def _copy_file(self, source_fn, dest_fn):
        with open(source_fn, 'r') as source_file:
            with open(dest_fn, 'w') as dest_file:
                dest_file.write(source_file.read())

    def get_file(self, source_fn, dest_fn):
        logging.info('get file ' + source_fn + ' from ' + dest_fn)
        self._copy_file(source_fn, dest_fn)

    def put_file(self, source_fn, dest_fn):
        logging.info('put file ' + source_fn + ' to ' + dest_fn)
        self._copy_file(source_fn, dest_fn)

    def get_fn(self, fn):
        return os.path.expanduser(os.path.join(self.base_dir, fn))
