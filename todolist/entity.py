import re


class Entity:

    def __init__(self, line=''):
        self.line = line.rstrip('\n')

    def __str__(self):
        return self.line

    def is_done(self):
        return re.search(r'^[xX]', self.line)
