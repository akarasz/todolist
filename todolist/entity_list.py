from todolist.entity import Entity


def read_file(input_fn):
    result = []

    with open(input_fn, 'r') as input_file:
        for line in input_file:
            result.append(Entity(line))

    return result

def write_file(output_fn, entity_list):
    with open(output_fn, 'w') as output_file:
        for entity in entity_list:
            output_file.write(str(entity) + '\n')
