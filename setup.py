from setuptools import setup

setup(
    name='todolist',
    version='0.0',
    description='scheduled task to manage todo.txt styled todo file on dropbox',
    url='http://github.com/akarasz/todolist',
    author='András Kárász',
    author_email='karaszandris@gmail.com',
    license='BSD',
    scripts=['bin/todolist-do_stuff.py'],
    packages=['todolist', 'todolist.storage'],
    install_requires=[
        'dropbox'
    ],
    zip_safe=False)
